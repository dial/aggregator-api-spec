[![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](http://creativecommons.org/licenses/by/4.0/)

## License and Acknowledgements

**Unless otherwise indicated, this content stewarded by the Digital Impact Alliance at United Nations Foundation and build by a consortium of specialists, collectively known as "Core Mobile Services API Suite Developer Specification" (CMS-API), is licensed under a Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.**

The work "CMS-API Contribution Policy" was adapted from "LibreHealth Contribution Policy" by Software Freedom Conservancy, Inc., used under the Creative Commons 4.0 International Attribution License (CC BY 4.0).

The work “CMS-API Code of Conduct” is adapted from the Contributor Covenant, version 1.4, available at http://contributor-covenant.org/version/1/4

The work "CMS-API Privacy Policy" was adapted from "LibreHealth Privacy Policy" by Software Freedom Conservancy, Inc., used under the Creative Commons 4.0 International Attribution License (CC BY 4.0).
