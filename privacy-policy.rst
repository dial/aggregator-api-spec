.. _privacy-policy:

Privacy Policy
==============

The Core Mobile Services API Suite Developer Specification is a free & open source project organized by a worldwide group of volunteers. Our challenges and goals are large. For that reason, we have a very permissive (non) privacy policy that mirrors our public mission.

Please assume that everything you contribute intentionally to the Core Mobile Services API Suite Developer Specification (CMS-API) is public. This includes messages on public forums, wikis, online discussions of all sorts, and software or documentation that you post to our websites.

Because we value mutual personal respect, disclosure of interests, and openness of expression, even your Open Source Center profile is public information. There will be cases where you are invited to share private information for various purposes, and denote that information as such. In those cases, we commit to only disclosing that information without your permission upon receipt of valid legal orders, and whenever possible, following notice from the Open Source Center to you.

Although what you disclose here is public, we are all limited by copyright and patent law in how we may use your contributions. Therefore, the Open Source Center only accepts voluntary contributions of software and documentation that are expressly licensed to the Open Source Center under an approved open source license. Refer to the Open Source Center Contribution Policy for additional details.

The Open Source Center welcomes your questions or comments regarding this Privacy Policy. Send them to dmccann@digitalimpactalliance.org.
