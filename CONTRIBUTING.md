## Contributing to this documentation

This website is build by readthedocs from a [gitlab repository](https://gitlab.com/dial/aggregator-api-spec).  Contributions can be made in the form of pull requests to this repository, following the same licensing considerations as above.
