# Core Mobile Services API Suite Developer Specification

This repository serves as the API technical specification for a suite of APIs to ingrate application-layer software with core mobile services exposed by mobile network operators (MNOs), in a standardized way. The goal is to reduce the need for multiple integration implementations at the application layer to interface with a common set of services exposed by MNOs.
